# frozen_string_literal: true

require_relative 'graph.rb'
require_relative 'point.rb'

# Module for reading graph from file
module FileReader
  def self.read_graph(filename)
    file_data = File.open(filename, 'r', &:read)
    conditions = file_data.lines
    # Remove count line from conditions
    conditions.shift
    vertices_information = parse_vertices_information(conditions)
    return create_graph(vertices_information)
  end

  def self.parse_vertices_information(conditions)
    vertices_information = conditions.map do |condition|
      splitted_condition = condition.split.map(&:to_i)
      Point.new(splitted_condition[1], splitted_condition[2])
    end
    return vertices_information
  end

  def self.create_graph(vertices_information)
    count = vertices_information.length
    graph = Graph.new(count)
    Range.new(0, count - 1).each do |i|
      first_vertex = vertices_information[i]
      Range.new(i + 1, count - 1).each do |j|
        second_vertex = vertices_information[j]
        graph[i, j] = first_vertex.calculate_distance(second_vertex)
      end
    end
    return graph
  end
end
