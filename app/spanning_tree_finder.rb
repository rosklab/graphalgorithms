# frozen_string_literal: true

require_relative 'graph'
require_relative 'edge'
require_relative 'file_writer'

# Class for finding bounded diameter minimum spanning tree
class SpanningTreeFinder
  def initialize(graph, diameter_limit, max_weight)
    @graph = graph
    @diameter_limit = diameter_limit
    @max_weight = max_weight
    @vertex_count = graph.vertex_count
    @best_tree_edges = []
    @best_tree_edges_sum = 0
    @best_max_distance = 0
    @vertex_range = Range.new(0, @vertex_count - 1)
    # Fields for finding one solution
    @tree_edges = []
    @tree_edges_sum = 0
    @used_vertices = []
    @suitable_vertices = @vertex_range.to_a
    @distance_matrix = create_square_matrix(@vertex_count, 0)
    # Max edge distance
    @max_distance = 0
    # TODO: Customize tabu search
    @used_tree = []
  end

  def local_search_spanning_tree(repeat_count, step_count, neighbour_count)
    Range.new(0, repeat_count - 1).each do |repeat|
      puts("Current repeat: #{repeat}")
      find_random_greedy_spaning_tree
      Range.new(0, step_count - 1).each do |step|
        puts("Current step: #{step}")
        break unless find_suitable_neighbour(neighbour_count)

        update_best_solution
      end
    end
    return @best_tree_edges, @best_max_distance
  end

  def find_greedy_spaning_tree
    edges = @graph.edges
    puts("Edge count: #{edges.length}")
    edges.shuffle[1..@vertex_count].each.with_index do |edge, index|
      puts("Edge index: #{index}")
      reset_spanning_tree_information
      update_spanning_tree(edge)
      bad_solution = false
      while @used_vertices.length < @vertex_count
        update_spanning_tree(find_next_edge)
        if @max_weight != 0 && @tree_edges_sum > @max_weight
          bad_solution = true
          break
        end

        next if @best_tree_edges.empty?

        if @tree_edges_sum > @best_tree_edges_sum
          bad_solution = true
          break
        end
      end
      next if bad_solution

      update_best_solution
    end
    return @best_tree_edges, @best_max_distance
  end

  private

  def find_random_greedy_spaning_tree
    reset_spanning_tree_information
    update_spanning_tree(find_first_edge_random)
    while @used_vertices.length < @vertex_count
      update_spanning_tree(find_next_edge_random)
      # update_spanning_tree(find_next_edge)
    end
    update_best_solution
  end

  def find_first_edge_random
    edges = @graph.edges
    edge = edges[Random.rand(edges.length)]
    return edge
  end

  def find_first_edge
    return @graph.edges.min do |first_edge, second_edge|
      first_edge.weight <=> second_edge.weight
    end
  end

  def find_next_edge
    min_edge = nil
    @used_vertices.each do |vertex|
      # Add decrease @diameter_limit logic
      next if @distance_matrix[vertex].max == @diameter_limit

      @suitable_vertices.each do |to_vertex|
        weight = @graph[vertex, to_vertex]
        next if weight == -1

        if min_edge.nil? || min_edge.weight > weight
          min_edge = Edge.new(vertex, to_vertex, weight)
        end
      end
    end

    return min_edge
  end

  def find_next_edge_random
    return find_next_edge if Random.rand > 0.8

    from_vertex = nil
    to_vertex = nil
    weight = nil
    loop do
      from_vertex = @used_vertices[Random.rand(@used_vertices.length)]
      # Add decrease @diameter_limit logic
      break if @distance_matrix[from_vertex].max < @diameter_limit
    end
    loop do
      to_vertex = @suitable_vertices[Random.rand(@suitable_vertices.length)]
      weight = @graph[from_vertex, to_vertex]
      break if weight != -1
    end
    return Edge.new(from_vertex, to_vertex, weight)
  end

  def reset_spanning_tree_information
    @tree_edges.clear
    @tree_edges_sum = 0
    @used_vertices.clear
    @max_distance = 0
    @suitable_vertices = @vertex_range.to_a
    reset_distance_matrix
  end

  def update_spanning_tree(edge)
    add_edge_in_tree(edge)
    # Update distance matrix before used vertices
    update_distance_matrix(edge)
    update_used_vertices(edge)
    update_suitable_vertices(edge)
  end

  def update_distance_matrix(edge)
    # Use from_vertex and to_vertex, because in new Edge have this vertices sequence
    last_vertex = edge.from_vertex
    new_vertex = edge.to_vertex
    @distance_matrix[last_vertex][new_vertex] = 1
    @distance_matrix[new_vertex][last_vertex] = 1
    @used_vertices.each do |vertex|
      next if vertex == last_vertex

      distance = @distance_matrix[vertex][last_vertex] + 1
      @distance_matrix[vertex][new_vertex] = distance
      @distance_matrix[new_vertex][vertex] = distance
      @max_distance = distance if @max_distance < distance
    end
  end

  def update_used_vertices(edge)
    @used_vertices << edge.from_vertex unless @used_vertices.include?(edge.from_vertex)
    @used_vertices << edge.to_vertex unless @used_vertices.include?(edge.to_vertex)
  end

  def update_suitable_vertices(edge)
    @suitable_vertices.delete(edge.from_vertex)
    @suitable_vertices.delete(edge.to_vertex)
  end

  def update_best_solution
    if @best_tree_edges.empty? ||
       @best_tree_edges_sum > @tree_edges_sum
      @best_tree_edges_sum = @tree_edges_sum
      @best_max_distance = @max_distance
      @best_tree_edges = Array.new(@tree_edges)
      puts("Best solution update, weight: #{@best_tree_edges_sum}")
      FileWriter.write_spanning_tree_solution("result/spanning_tree_#{@vertex_count}.txt",
                                              @best_tree_edges, @best_max_distance,
                                              @vertex_count)
    end
  end

  def create_square_matrix(size, default_value)
    return Array.new(size) { Array.new(size, default_value) }
  end

  def find_suitable_neighbour(neighbour_count)
    puts('Find suitable neighbour')
    sorted_edges = @tree_edges.sort do |first_edge, second_edge|
      first_edge.weight <=> second_edge.weight
    end
    # edges = Array.new(@tree_edges)
    visited_neighbors = 0
    # Add shuffle if array not sorted
    sorted_edges.reverse_each do |edge|
      delete_edge_from_tree(edge)
      first_component, removed_edges = find_accessible_vertices(edge.from_vertex, [], [])
      second_component, = find_accessible_vertices(edge.to_vertex, [], removed_edges)
      if first_component.length == 1 || second_component.length == 1
        add_edge_in_tree(edge)
        next
      end
      return true if move_to_neighbour(first_component, second_component, edge.weight)

      add_edge_in_tree(edge)
      visited_neighbors += 1
      return false if visited_neighbors >= neighbour_count
    end
    return false
  end

  def find_accessible_vertices(vertex, accessible_vertices, removed_edges)
    accessible_vertices << vertex
    @tree_edges.each do |edge|
      next if removed_edges.include?(edge)

      if edge.from_vertex == vertex # && !accessible_vertices.include?(edge.to_vertex)
        removed_edges << edge
        find_accessible_vertices(edge.to_vertex, accessible_vertices, removed_edges)
      end
      if edge.to_vertex == vertex # && !accessible_vertices.include?(edge.from_vertex)
        removed_edges << edge
        find_accessible_vertices(edge.from_vertex, accessible_vertices, removed_edges)
      end
    end
    return accessible_vertices, removed_edges
  end

  def move_to_neighbour(first_component, second_component, min_weight)
    first_component.each do |first_vertex|
      second_component.each do |second_vertex|
        weight = @graph[first_vertex, second_vertex]
        # Check can remove weight == -1
        next if weight == -1 || weight >= min_weight

        edge = Edge.new(first_vertex, second_vertex, weight)
        add_edge_in_tree(edge)
        diameter = calculate_diameter
        if diameter <= @diameter_limit
          @max_distance = diameter
          return true
        end
        delete_edge_from_tree(edge)
      end
    end
    return false
  end

  def calculate_diameter
    reset_distance_matrix
    # Use this field after greedy algorithm
    @used_vertices.clear
    edges = Array.new(@tree_edges)
    first_edge = edges.pop
    update_distance_matrix(first_edge)
    update_used_vertices(first_edge)
    fill_distance_matrix(edges)
    rows_max = @distance_matrix.map(&:max)
    return rows_max.max
  end

  # Method modified edges
  def fill_distance_matrix(edges)
    while @used_vertices.length < @vertex_count
      suitable_edge = nil
      correct_sequence = true
      edges.each do |edge|
        if @used_vertices.include?(edge.from_vertex)
          suitable_edge = edge
          break
        end
        if @used_vertices.include?(edge.to_vertex)
          suitable_edge = edge
          correct_sequence = false
          break
        end
      end
      return @diameter_limit + 1 if suitable_edge.nil?

      edges.delete(suitable_edge)
      unless correct_sequence
        suitable_edge = Edge.new(suitable_edge.to_vertex, suitable_edge.from_vertex,
                                 suitable_edge.weight)
      end
      update_distance_matrix(suitable_edge)
      update_used_vertices(suitable_edge)
    end
  end

  def delete_edge_from_tree(edge)
    @tree_edges.delete(edge)
    @tree_edges_sum -= edge.weight
  end

  def add_edge_in_tree(edge)
    @tree_edges << edge
    @tree_edges_sum += edge.weight
  end

  def reset_distance_matrix
    @vertex_range.each do |row|
      @vertex_range.each { |column| @distance_matrix[row][column] = 0 }
    end
  end
end
