# frozen_string_literal: true

require_relative 'edge.rb'

# Module for write solutions of algorithms
module FileWriter
  def self.write_spanning_tree_solution(filename, tree_edges, max_distance, vertex_count)
    tree_weight = tree_edges.sum(&:weight)
    data = "c Вес дерева = #{tree_weight}, диаметр = #{max_distance}\n"
    data += "p edge #{vertex_count} #{tree_edges.length}\n"
    sorted_tree_edges = sort_tree_edges(tree_edges)
    edges_data = sorted_tree_edges.map do |edge|
      "e #{edge.from_vertex + 1} #{edge.to_vertex + 1}"
    end
    data += edges_data.join("\n")
    File.open(filename, 'w') { |file| file.write(data) }
  end

  def self.sort_tree_edges(tree_edges)
    transformed_tree_edges = tree_edges.map do |edge|
      if edge.from_vertex > edge.to_vertex
        Edge.new(edge.to_vertex, edge.from_vertex, edge.weight)
      else
        Edge.new(edge.from_vertex, edge.to_vertex, edge.weight)
      end
    end
    return transformed_tree_edges.sort do |firts_edge, second_edge|
      if firts_edge.from_vertex == second_edge.from_vertex
        firts_edge.to_vertex <=> second_edge.to_vertex
      else
        firts_edge.from_vertex <=> second_edge.from_vertex
      end
    end
  end
end
