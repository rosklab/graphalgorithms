# frozen_string_literal: true

# Class describes point in two-dimensional space
class Point
  attr_accessor :x, :y

  def initialize(x_coordinate = 0, y_coordinate = 0)
    @x = x_coordinate
    @y = y_coordinate
  end

  # Calculate distance in taxicab geometry
  def calculate_distance(other_point)
    (@x - other_point.x).abs + (@y - other_point.y).abs
  end
end
