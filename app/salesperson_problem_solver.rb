# frozen_string_literal: true

require_relative 'graph.rb'
require_relative 'edge.rb'

# Class for solve 2-peripatetic salesperson problem
class SalespersonProblemSolver
  attr_reader :graph, :first_vertex, :edge_path, :blocked_edges,
              :first_edge_path, :second_edge_path

  def initialize(graph, first_vertex)
    @graph = graph
    @first_vertex = first_vertex
    @edge_path = []
    @visited_vertices = [first_vertex]
    @blocked_edges = []
    @first_edge_path = []
    @second_edge_path = []
  end

  def find_greedy_solution
    find_greedy_path(@first_vertex)
    @first_edge_path = @edge_path
    @edge_path = []
    @visited_vertices = [@first_vertex]
    find_greedy_path(@first_vertex)
    @second_edge_path = @edge_path
    return @first_edge_path, @second_edge_path
  end

  private

  def find_greedy_path(from_vertex)
    return check_last_edge(from_vertex) if @visited_vertices.length == @graph.vertex_count

    unsuitable_vertices = []
    loop do
      edge = find_edge_with_min_weight(from_vertex, unsuitable_vertices)
      return false if edge.nil?

      @edge_path.push(edge)
      @blocked_edges.push(edge)
      @visited_vertices.push(edge.to_vertex)
      unsuitable_vertices.push(edge.to_vertex)
      break if find_greedy_path(edge.to_vertex)

      @edge_path.pop
      @blocked_edges.pop
      @visited_vertices.pop
    end

    return true
  end

  def find_edge_with_min_weight(from_vertex, unsuitable_vertices)
    blocked_vertices = collect_blocked_vertices(from_vertex)
    min_weight = nil
    to_vertex = nil
    Range.new(0, @graph.vertex_count - 1).each do |i|
      next if @visited_vertices.include?(i) ||
              blocked_vertices.include?(i) ||
              unsuitable_vertices.include?(i)

      weight = @graph[from_vertex, i]
      next if weight == -1

      if min_weight.nil? || min_weight > weight
        min_weight = weight
        to_vertex = i
      end
    end
    return nil if min_weight.nil? || to_vertex.nil?

    return Edge.new(from_vertex, to_vertex, min_weight)
  end

  def collect_blocked_vertices(from_vertex)
    blocked_vertices = []
    @blocked_edges.each do |edge|
      blocked_vertices.push(edge.to_vertex) if edge.from_vertex == from_vertex
      blocked_vertices.push(edge.from_vertex) if edge.to_vertex == from_vertex
    end
    return blocked_vertices
  end

  def check_last_edge(from_vertex)
    weight = @graph[from_vertex, @first_vertex]
    edge = Edge.new(from_vertex, @first_vertex, weight)
    return false if @blocked_edges.include?(edge)

    @edge_path.push(edge)
    @blocked_edges.push(edge)
    return true
  end
end
