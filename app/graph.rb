# frozen_string_literal: true

require_relative 'edge.rb'

# Class describes graph
class Graph
  attr_reader :vertex_count, :directed

  def initialize(vertex_count, directed = false)
    @vertex_count = vertex_count
    @adjacency_matrix = Array.new(vertex_count) { Array.new(vertex_count, -1) }
    @directed = directed
  end

  def [](from_vertex, to_vertex)
    @adjacency_matrix[from_vertex][to_vertex]
  end

  def []=(from_vertex, to_vertex, weight)
    @adjacency_matrix[from_vertex][to_vertex] = weight
    @adjacency_matrix[to_vertex][from_vertex] = weight unless @directed
  end

  def edges
    if @directed
      edges_for_directed_graph
    else
      edges_for_undirected_graph
    end
  end

  def edge(from_vertex, to_vertex, directed = false)
    limit = Range.new(0, vertex_count - 1)
    return nil if !limit.include?(from_vertex) || !limit.include?(to_vertex)

    weight = @adjacency_matrix[from_vertex][to_vertex]
    return nil if weight == -1

    Edge.new(from_vertex, to_vertex, weight, directed)
  end

  private

  def edges_for_directed_graph
    edges = []

    Range.new(0, @vertex_count - 1).each do |from_vertex|
      Range.new(0, @vertex_count - 1).each do |to_vertex|
        edge = edge(from_vertex, to_vertex, true)
        next if edge.nil?

        edges.push(edge)
      end
    end

    edges
  end

  def edges_for_undirected_graph
    edges = []

    Range.new(0, @vertex_count - 1).each do |from_vertex|
      Range.new(from_vertex, @vertex_count - 1).each do |to_vertex|
        edge = edge(from_vertex, to_vertex)
        next if edge.nil?

        edges.push(edge)
      end
    end

    edges
  end
end
