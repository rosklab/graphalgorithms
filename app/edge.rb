# frozen_string_literal: true

# Class describe edge of graph
class Edge
  attr_accessor :from_vertex, :to_vertex, :weight, :directed

  def initialize(from_vertex, to_vertex, weight, directed = false)
    @from_vertex = from_vertex
    @to_vertex = to_vertex
    @weight = weight
    @directed = directed
  end

  def ==(other)
    return false if other.class != Edge ||
                    @directed != other.directed ||
                    @weight != other.weight

    if @directed
      return @from_vertex == other.from_vertex &&
             @to_vertex == other.to_vertex
    else
      return ((@from_vertex == other.from_vertex &&
             @to_vertex == other.to_vertex) ||
             (@from_vertex == other.to_vertex &&
             @to_vertex == other.from_vertex))
    end
  end

  def to_s
    return 'Edge {'\
      "from: #{@from_vertex}, "\
      "to: #{@to_vertex}, "\
      "weight: #{@weight}, "\
      "directed: #{@directed}"\
      '}'
  end
end
