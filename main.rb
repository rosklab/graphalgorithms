# frozen_string_literal: true

require_relative 'app/file_reader'
require_relative 'app/file_writer'
require_relative 'app/salesperson_problem_solver'
require_relative 'app/spanning_tree_finder'

def run_salesperson_problem_solver(graph)
  salesperson_problem_solver = SalespersonProblemSolver.new(graph, 0)
  first_path, second_path = salesperson_problem_solver.find_greedy_solution

  first_sum = first_path.sum(&:weight)
  second_sum = second_path.sum(&:weight)
  total_sum = first_sum + second_sum
  puts("First path edge count: #{first_path.length}")
  puts("Second path edge count: #{second_path.length}")
  puts("First: #{first_sum}, second: #{second_sum}, total: #{total_sum}")

  path_string = ''
  first_path.each do |edge|
    path_string += "#{edge.from_vertex + 1} "
  end
  puts("First path: #{path_string}")

  path_string = ''
  second_path.each do |edge|
    path_string += "#{edge.from_vertex + 1} "
  end
  puts("Second path: #{path_string}")
end

def run_spanning_tree_finder(graph, weight)
  vertex_count = graph.vertex_count
  spanning_tree_finder = SpanningTreeFinder.new(graph, find_diameter(vertex_count),
                                                weight)
  # Example of using greedy algorithm
  # tree_edges, max_distance = spanning_tree_finder.find_greedy_spaning_tree
  tree_edges, max_distance = spanning_tree_finder.local_search_spanning_tree(5, 1000, 50)
  # File write in algorithm
  # FileWriter.write_spanning_tree_solution("result/spanning_tree_#{vertex_count}.txt",
  #                                         tree_edges, max_distance, vertex_count)
  puts('Tree edges count:', tree_edges.length)
  puts('Tree edges sum:', tree_edges.sum(&:weight))
  puts('Max distance:', max_distance)
end

def find_diameter(vertex_count)
  return vertex_count / 50 + 5
end

if ARGV.length < 2
  puts('Two parameters are needed to run the application')
  puts('First parameter: problem name')
  puts('Second parameter: graph name')
  return
end

problem = ARGV[0]
vertex_count = ARGV[1]
# nil transform to 0
weight = ARGV[2].to_i

graph = FileReader.read_graph("data/Taxicab_#{vertex_count}.txt")

if problem == 'sp'
  run_salesperson_problem_solver(graph)
elsif problem == 'st'
  run_spanning_tree_finder(graph, weight)
end
