# To do list

## Need to do

### Classes
* SpaningTreeFinder
  * Implement greedy algorithm for finding minimum spanning tree

### Modules

## Already done

### Classes
* Point
  * Implement Point class
* Edge
  * Implement Edge class
* SalespersonProblemSolver
  * Implement greedy algorithm for 2-peripatetic salesperson problem

### Modules
* FileReader
  * Graph information reader
